using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class CountDown : Singleton<CountDown>
{
    public TextMeshProUGUI timeText;
    // Start is called before the first frame update
    public float timeRemaining = 10;
    public bool timerIsRunning = false;
    public bool isFinalStage;
    public bool once;
    private void Start()
    {
        // Starts the timer automatically
        //timerIsRunning = true;
    }
    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                if (isFinalStage)
                {
                    Debug.Log("is final stage");
                    if (once)
                    {
                    Debug.Log("not once");
                        once = false;
                        DotManager.main.OnLose.Invoke();
                    }
                }
                else
                {
                    DataManager.main.SetNewTime(GetCurrentTimeWithFormat(timeRemaining));
                    SceneManager.LoadScene("lastScene");
                }
            }
            DisplayTime(timeRemaining);
        }
    }
    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timeText.text = string.Format("{0:00} {1:00}", minutes, seconds);
    }
    public string GetCurrentTimeWithFormat(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        return string.Format("{0:00} {1:00}", minutes, seconds);
    }
    public void Reset()
    {
        SceneManager.LoadScene("home");
    }
}
