using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    public string firstTime = "";
    public string secondTime = "";
    public bool gano1;
    public bool gano2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetNewTime(string _time)
    {
        if (firstTime != "")
        {
            secondTime = _time;
        }
        else
        {
            firstTime = _time;
        }
    }
}
