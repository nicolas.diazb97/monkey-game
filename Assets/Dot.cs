using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dot : MonoBehaviour
{
    public bool able = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Interact()
    {
        if (Input.GetMouseButton(0))
        {
            DotManager.main.CheckWin();
            SetFriends(DotManager.main.dots.IndexOf(this));
            able = false;
            GetComponent<Image>().color = new Color32(0, 0, 0, 255);
            Debug.LogError("interact");
        }
    }
    public void SetFriends(int index)
    {
        if (index - 2 > 0 && index + 2 < DotManager.main.dots.Count)
        {
            DotManager.main.dots[index - 2].InteractedByFriend();
            DotManager.main.dots[index - 1].InteractedByFriend();
            DotManager.main.dots[index + 1].InteractedByFriend();
            DotManager.main.dots[index + 2].InteractedByFriend();
        }
    }
    public void InteractedByFriend()
    {
        able = false;
        GetComponent<Image>().color = new Color32(0, 0, 0, 255);
        Debug.LogError("interact");
    }
}
