using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class DotManager : Singleton<DotManager>
{
    public UnityEvent OnWin;
    public UnityEvent OnLose;
    public List<Dot> dots;
    public UnityEvent onRewardWin;
    bool once = true;
    // Start is called before the first frame update
    void Start()
    {
        dots = GetComponentsInChildren<Dot>().ToList();
        OnWin.AddListener(() =>
        {
            DataManager.main.SetNewTime(CountDown.main.GetCurrentTimeWithFormat(CountDown.main.timeRemaining));
            CountDown.main.timerIsRunning = false;
            if (DataManager.main.secondTime == "")
            {
                DataManager.main.gano1 = true;
                SceneManager.LoadScene("lastScene");
            }
            else
            {
                DataManager.main.gano2 = true;

            }
            if (DataManager.main.secondTime != "")
            {
                if (DataManager.main.gano1 && DataManager.main.gano2)
                {
                    onRewardWin.Invoke();
                }
                else
                {
                    OnLose.Invoke();
                }
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {

            //if (EventSystem.current.IsPointerOverGameObject())
            //{
            //    if (EventSystem.current.game.GetComponent<Dot>())
            //        Debug.Log("left-click over a GUI element!");

            //}
        }
    }
    public void CheckWin()
    {
        if (once)
        {
            once = false;
            CountDown.main.timerIsRunning = true;
            CountDown.main.once = true;
        }
        List<Dot> matchedData = dots.Where(d => d.able == true).ToList();
        //Debug.LogError(matchedData.Count + " faltantes");
        if (matchedData.Count() <= 5)
            OnWin.Invoke();
    }
}
