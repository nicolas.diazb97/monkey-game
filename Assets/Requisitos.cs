using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Requisitos : MonoBehaviour
{
    public bool correo;
    public bool mail;
    public bool check1;
    public bool check2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetBoolFromIndex(int _index)
    {
        if (_index == 0)
        {
            correo = true;
        }
        if (_index == 1)
        {
            mail = true;
        }
        if (_index == 2)
        {
            check1 = true;
        }
        if (_index == 3)
        {
            check2 = true;
        }
    }
    public void Check()
    {
        Debug.LogError("checking");
        if (correo && mail && check1 && check2)
        {
        Debug.LogError("checked");
            GetComponent<Button>().interactable = true;
        }
    }
}
