using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResultsScreen : MonoBehaviour
{
    public TextMeshProUGUI wall1;
    public TextMeshProUGUI wall2;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
    }
    // Update is called once per frame
    void Update()
    {
        wall1.text = DataManager.main.firstTime;
        wall2.text = DataManager.main.secondTime;
    }
}
